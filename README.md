# AutomaticScheduleGenerator

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.
Link: `https://automatic-schedule-generator.herokuapp.com` 
## Development server - follow the commands to run project on your local machine:

Run `npm install` to install dependencies.
Run `npm start` or `ng serve` to start project on your local machine.
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
