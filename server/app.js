const createError = require('http-errors');
const express = require('express');
const path = require('path');

const app = express();
console.log('it worked');
app.use(express.static(path.join('dist/automatic-schedule-generator')));

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  console.log(path.join('dist/automatic-schedule-generator/index.html'));
  res.sendFile(path.join('dist/automatic-schedule-generator/index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // render the error page
  res.status(err.status || 500);
  res.json({message: 'NOT FOUND'});
});

module.exports = app;
