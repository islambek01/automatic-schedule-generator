export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyC9H0AV2bqqmnIqz-c5rFjstkSfsL5M3ME',
    authDomain: 'automatic-schedule-generator.firebaseapp.com',
    databaseURL: 'https://automatic-schedule-generator.firebaseio.com',
    projectId: 'automatic-schedule-generator',
    storageBucket: 'automatic-schedule-generator.appspot.com',
    messagingSenderId: '739741984332',
    appId: '1:739741984332:web:f53726bed2c703b3'
  }};
