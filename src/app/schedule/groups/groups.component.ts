import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {GroupService} from '../../group.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
  groupForm = this.fb.group({
    name: ['', Validators.required],
    size: [''],
    code: ['']
  });

  listOfGroups: any = [];
  groups: Observable<any>;

  constructor(private fb: FormBuilder,
              private groupService: GroupService) {
  }

  ngOnInit() {
    console.log('group on init');
    this.groupService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfGroups = data.sort((a, b) => (a.name > b.name) ? 1 : -1);;
    }, error => {
      console.log(error);
    });
  }

  onAdd() {
    const group = this.groupForm.value;
    const code = this.listOfGroups.length;
    group.code = code;
    // console.warn(group);
    this.groupService.createNote(group).then((data) => {
      console.log('group was created:', data);
    });
  }

  remove(groupId) {
    this.groupService.deleteNote(groupId).then((data) => {
      console.log(data);
    });
  }
}
