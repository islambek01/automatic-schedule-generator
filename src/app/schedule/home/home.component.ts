import { Component, OnInit } from '@angular/core';
import { Group } from '../../models/group';
import { Teacher } from '../../models/teacher';
import { Room } from '../../models/room';
import { Lesson } from '../../models/lesson';
import { Chromosome } from '../../models/chromosome';
import { ListLesson } from '../../models/list-lesson';
export type EditorType = 'group' | 'teacher' | 'room' | 'lesson' | 'showSchedule' | 'listScheduleEditor';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  lesson: Lesson = new Lesson();
  group: Group = new Group();
  teacher: Teacher = new Teacher();
  room: Room = new Room();
  listOfLessons: ListLesson = new ListLesson();
  numberOfRooms: any = new Array<number>(4);
  numberOfGroups: any = new Array<number>(5);
  numberOfTeachers: any = new Array<number>(5);
  dayHours: any = new Array<number>(8);
  info: { space: any, lessons: any};
  conflicts: { total: any, notLocated: any };
  bestSchedule: { lessons: any };
  scheduleByGroups: { lessons: any };
  scheduleByTeachers: { lessons: any };

  editor: EditorType = 'group';

  constructor() { }

  get showGroupEditor() {
    return this.editor === 'group';
  }

  get showTeacherEditor() {
    return this.editor === 'teacher';
  }

  get showRoomEditor() {
    return this.editor === 'room';
  }

  get showLessonEditor() {
    return this.editor === 'lesson';
  }
  get showScheduleEditor() {
    return this.editor === 'showSchedule';
  }
  get listScheduleEditor() {
    return this.editor === 'listScheduleEditor';
  }

  toggleEditor(type: EditorType) {
    this.editor = type;
  }

  ngOnInit() {}
  // ngOnInit() {
  //   const info = {space: 160, lessons: 150};
  //   this.info = info;
  //
  //   for (let i = 1; i < 2; i++) {
  //     this.listOfLessons.lessons.push(this.makeLesson(i));
  //   }
  //
  //   const allLessons = this.listOfLessons;
  //   console.log(allLessons);
  //
  //   let maxFitnessValue = 0;
  //
  //   const population = [];
  //   for (let k = 1; k < 10; k++) {
  //     const chromosome = this.makeNewChromosome(allLessons);
  //
  //     if (chromosome.fitness > maxFitnessValue) {
  //       maxFitnessValue = chromosome.fitness;
  //       this.bestSchedule = chromosome;
  //     }
  //
  //     population.push(chromosome);
  //   }
  //   console.log(population);
  //
  //   console.log('Max Fitness Value: ', maxFitnessValue);
  //   console.log('Please wait for 15 seconds to see final result...');
  //
  //   for (let i = 1; i < 5; i++) {
  //     // select two parent chromosomes randomly to make crossover
  //     const first = this.getRandomInt(1000);
  //     const second = this.getRandomInt(1000);
  //
  //     if (first !== second) {
  //       const parent1 = population[first];
  //       const parent2 = population[second];
  //
  //       if (parent1 && parent2) {
  //         const newOffSprings = this.makeCrossover(parent1, parent2);
  //         const offSpring1 = newOffSprings[0];
  //         const offSpring2 = newOffSprings[1];
  //
  //         const newChromosome1 = this.makeMutation(offSpring1);
  //         const newChromosome2 = this.makeMutation(offSpring2);
  //
  //         const fitnessVal1 = this.calculateFitness(newChromosome1);
  //         newChromosome1.fitness = fitnessVal1;
  //
  //         const fitnessVal2 = this.calculateFitness(newChromosome2);
  //         newChromosome2.fitness = fitnessVal2;
  //
  //         // console.log(offSpring2.fitness, ' :S: ', newChromosome2.fitness)
  //         if (offSpring1.fitness < newChromosome1.fitness) {
  //           // console.log(offSpring1.fitness, ' :VS: ', newChromosome1.fitness);
  //         }
  //
  //         if (offSpring1.fitness > maxFitnessValue) {
  //           maxFitnessValue = offSpring1.fitness;
  //           this.bestSchedule = offSpring1;
  //         }
  //         if (offSpring2.fitness > maxFitnessValue) {
  //           maxFitnessValue = offSpring2.fitness;
  //           this.bestSchedule = offSpring2;
  //         }
  //
  //         if (parent1.fitness < offSpring1.fitness) {
  //           population[first] = offSpring1;
  //           // console.log(parent1.fitness, ' 1VS1 ', offSpring1.fitness);
  //         } else if (parent1.fitness < offSpring2.fitness) {
  //           population[first] = offSpring2;
  //           // console.log(parent1.fitness, ' 1VS2 ', offSpring2.fitness);
  //         } else if (parent2.fitness < offSpring1.fitness) {
  //           population[second] = offSpring1;
  //           // console.log(parent2.fitness, ' 2VS1 ', offSpring1.fitness);
  //         } else if (parent2.fitness < offSpring2.fitness) {
  //           population[second] = offSpring2;
  //           // console.log(parent2.fitness, ' 2VS2 ', offSpring2.fitness);
  //         }
  //       }
  //
  //     }
  //
  //     // select chromosome randomly to make mutation
  //     // const mutationPosition = this.getRandomInt(1000);
  //     // const randomForMutation = population[mutationPosition];
  //     // // if (randomForMutation) {
  //     //   const newChromosome = this.makeMutation(randomForMutation);
  //     //
  //     //   const fitnessValue = this.calculateFitness(newChromosome);
  //     //   newChromosome.fitness = fitnessValue;
  //     //
  //     //   if (randomForMutation.fitness < newChromosome.fitness) {
  //     //     console.log(randomForMutation.fitness, ' :VS: ', newChromosome.fitness);
  //     //     population[mutationPosition] = newChromosome;
  //     //   }
  //     // }
  //   }
  //
  //
  //   const bestSchedule = this.bestSchedule;
  //   console.log('Fitness Value after crossover & mutation: ', maxFitnessValue);
  //   console.log('The best schedule: ', bestSchedule);
  //
  //   let c = 0;
  //   const lessonOccurence = new Array(76);
  //   lessonOccurence.fill(0)
  //   bestSchedule.lessons.forEach(lesson => {
  //     if (lesson) {
  //       lessonOccurence[lesson.id]++;
  //       c++;
  //     }
  //   });
  //
  //   console.log('Empty fields in schedule: ', 160 - c);
  //   console.log('Occurence of lessons: ', lessonOccurence);
  //
  //   const conflicts = {total: this.logConflicts(bestSchedule), notLocated: 0};
  //   this.conflicts = conflicts;
  //
  //   const numberOfDays = 5;
  //   const numberOfHours = 8;
  //   const numberOfRooms = 4;
  //   const numberOfGroups = 5;
  //   const numberOfTeachers = 5;
  //
  //   const scheduleByGroups = {
  //     lessons: new Array<object>(numberOfGroups * numberOfHours * numberOfDays),
  //     fitness: 0
  //   };
  //
  //   const scheduleByTeachers = {
  //     lessons: new Array<object>(numberOfTeachers * numberOfHours * numberOfDays),
  //     fitness: 0
  //   };
  //
  //   let q = 0;
  //
  //   for (let i = 0; i < numberOfDays; i++) {
  //     for (let j = 0; j < numberOfRooms; j++) {
  //       for (let k = 0; k < numberOfHours; k++) {
  //         const position = i * numberOfHours * numberOfRooms + j * numberOfHours + k;
  //         const lesson = bestSchedule.lessons[position];
  //
  //         if (lesson) {
  //           lesson.roomNum = 'room' + (j + 1);
  //           const groupId = lesson.group.group.id;
  //           const teacherId = lesson.teacher.teacher.id;
  //
  //           const pos1 = i * numberOfHours * numberOfGroups + (groupId - 1) * numberOfHours + k;
  //           const pos2 = i * numberOfHours * numberOfTeachers + (teacherId - 1) * numberOfHours + k;
  //
  //           scheduleByGroups.lessons[pos1] = lesson;
  //           scheduleByTeachers.lessons[pos2] = lesson;
  //           q++;
  //         }
  //
  //       }
  //     }
  //   }
  //
  //   this.scheduleByGroups = scheduleByGroups;
  //   this.scheduleByTeachers = scheduleByTeachers;
  //
  //   console.log('Number of lessons in group and teacher schedule: ', q);
  //   console.log('Number of lessons not located in schedule: ', 150 - c);
  //   this.conflicts.notLocated = 150 - c;
  // }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  makeLesson(i) {
    const group = {
      name: 'group' + (i % 5 + 1),
      id: i % 5 + 1,
      size: 25
    };

    const teacher = {
      name: 'teacher' + (i % 5 + 1),
      id: i % 5 + 1,
      shortName: 'T.R.' + i % 5
    };

    const room = {
      name: 'room' + (i % 5 + 1),
      id: i % 5 + 1,
      isLab: i % 11 === 0,
      size: 30
    };

    const lesson = {
      name: 'lesson' + i,
      id: i,
      isLabRequired: i % 6 === 0,
      duration: 2,
      group: { group },
      teacher: { teacher },
      room: { room }
    };

    return lesson;
  }

  makeNewChromosome(allLessons) {
    const numberOfRooms = 4;
    const dayHours = 8;
    const days = 5;
    const chromosome = {
        lessons: new Array<object>(numberOfRooms * dayHours * days),
        fitness: 0
      };

    allLessons.lessons.forEach(lesson => {
      const duration = lesson.duration;
      const day = this.getRandomInt(days);
      const room = this.getRandomInt(numberOfRooms);
      const time = this.getRandomInt(dayHours + 1 - duration);
      const position = day * numberOfRooms * dayHours + room * dayHours + time;

      for (let k = duration - 1; k >= 0; k--) {
        chromosome.lessons[position + k] = lesson;
      }
    });

    const fitnessValue = this.calculateFitness(chromosome);
    chromosome.fitness = fitnessValue;

    return chromosome;
  }

  calculateFitness(chromosome) {
    let score = 0;
    const numberOfRooms = 4;
    const dayHours = 8;
    const daySize = numberOfRooms * dayHours;

    // iterate over days
    for (let i = 0; i < 5; i++) {
      // iterate over rooms
      for (let j = 0; j < numberOfRooms; j++) {
        // iterate over hours
        for (let k = 0; k < dayHours; k++) {
          const currentPosition = i * numberOfRooms * dayHours + j * dayHours + k;
          const currentLesson = chromosome.lessons[currentPosition];

          if (currentLesson) {
            // check overlap of teacher and groups
            let teacherOverLap = false;
            let groupOverLap = false;

            // iterate over rooms for one day
            for (let h = 0; h < numberOfRooms; h++) {
              const position = i * numberOfRooms * dayHours + h * dayHours + k;
              const lesson = chromosome.lessons[position];

              if (h !== j && lesson) {
                if (currentLesson.teacher.teacher.id === lesson.teacher.teacher.id) {
                  teacherOverLap = true;
                }
                if (currentLesson.group.group.id === lesson.group.group.id) {
                  groupOverLap = true;
                }
              }

              if (teacherOverLap && groupOverLap) {
                break;
              }
            }

            // check for teacher Overlap
            if (!teacherOverLap) {
              score++;
            }

            // check for group Overlap
            if (!groupOverLap) {
              score++;
            }

            // increment for room collapse
            score++;

            // increment for room has available seats
            score++;

            // increment for lesson requires lab and it is in lab or vise-versa
            score++;
          }

        }
      }
    }

    // calculate fitness value => scheduleScore/maxScore where maxScore=numberOfLessons*5
    const numberOfLessons = 150;
    const fitness = score / (numberOfLessons * 5);

    return fitness;
  }

  makeCrossover(parent1, parent2) {
    const numberOfCrossoverPoints = 2;
    const numberOfMutationSize = 2;
    const numberOfLessons = 150;
    const numberOfRooms = 4;
    const dayHours = 8;
    const days = 5;

    const offSpring1 = {
      lessons: new Array<{id: any}>(numberOfRooms * dayHours * days),
      fitness: 0
    };

    const offSpring2 = {
      lessons: new Array<{id: any}>(numberOfRooms * dayHours * days),
      fitness: 0
    };

    const crossoverPoint = this.getRandomInt(numberOfRooms * dayHours * days);

    for (let k = 0; k < crossoverPoint; k++) {

      const lesson1 = parent1.lessons[k];
      const lesson2 = parent2.lessons[k];

      let notChanged1 = true;
      let notChanged2 = true;

      if (lesson1) {
        let occurence1 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring2.lessons[q];

          if (lesson && lesson.id === lesson1.id) {
            occurence1++;
          }
        }

        if (occurence1 < lesson1.duration) {
          offSpring2.lessons[k] = lesson1;
          notChanged1 = false;
        }
      }
      if (notChanged1 && lesson2) {
        let occurence1 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring2.lessons[q];

          if (lesson && lesson.id === lesson2.id) {
            occurence1++;
          }
        }

        if (occurence1 < lesson2.duration) {
          offSpring2.lessons[k] = lesson2;
        }
      }

      if (lesson2) {
        let occurence2 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring1.lessons[q];

          if (lesson && lesson.id === lesson2.id) {
            occurence2++;
          }
        }

        if (occurence2 < lesson2.duration) {
          offSpring1.lessons[k] = lesson2;
          notChanged2 = false;
        }
      }
      if (notChanged2 && lesson1) {
        let occurence2 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring1.lessons[q];

          if (lesson && lesson.id === lesson1.id) {
            occurence2++;
          }
        }

        if (occurence2 < lesson1.duration) {
          offSpring1.lessons[k] = lesson1;
        }
      }

      // offSpring1.lessons[k] = parent2.lessons[k] ? parent2.lessons[k] : parent1.lessons[k];
      // offSpring2.lessons[k] = parent1.lessons[k] ? parent1.lessons[k] : parent2.lessons[k];
    }

    for (let k = crossoverPoint; k < numberOfRooms * dayHours * days; k++) {
      const lesson1 = parent1.lessons[k];
      const lesson2 = parent2.lessons[k];

      let notChanged1 = true;
      let notChanged2 = true;
      if (lesson1) {
        let occurence1 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring1.lessons[q];

          if (lesson && lesson.id === lesson1.id) {
            occurence1++;
          }
        }

        if (occurence1 < lesson1.duration) {
          offSpring1.lessons[k] = lesson1;
          notChanged1 = false;
        }
      }
      if (notChanged1 && lesson2) {
        let occurence1 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring1.lessons[q];

          if (lesson && lesson.id === lesson2.id) {
            occurence1++;
          }
        }

        if (occurence1 < lesson2.duration) {
          offSpring1.lessons[k] = lesson2;
        }
      }

      if (lesson2) {
        let occurence2 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring2.lessons[q];

          if (lesson && lesson.id === lesson2.id) {
            occurence2++;
          }
        }

        if (occurence2 < lesson2.duration) {
          offSpring2.lessons[k] = lesson2;
          notChanged2 = false;
        }
      }
      if (notChanged2 && lesson1) {
        let occurence2 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring2.lessons[q];

          if (lesson && lesson.id === lesson1.id) {
            occurence2++;
          }
        }

        if (occurence2 < lesson1.duration) {
          offSpring2.lessons[k] = lesson1;
        }
      }

      // offSpring1.lessons[k] = parent1.lessons[k] ? parent1.lessons[k] : parent2.lessons[k];
      // offSpring2.lessons[k] = parent2.lessons[k] ? parent2.lessons[k] : parent1.lessons[k];
    }

    const fitnessValue1 = this.calculateFitness(offSpring1);
    offSpring1.fitness = fitnessValue1;

    const fitnessValue2 = this.calculateFitness(offSpring2);
    offSpring2.fitness = fitnessValue2;

    return ([offSpring1, offSpring2]);
  }

  makeMutation(chromosome) {
    const mutationSize = 2;
    const numberOfRooms = 4;
    const dayHours = 8;
    const days = 5;

    for (let i = mutationSize; i >= 0; i--) {
      // select lesson randomly
      const pos1 = this.getRandomInt(numberOfRooms * dayHours * days);
      const pos2 = this.getRandomInt(numberOfRooms * dayHours * days);

      const lesson = chromosome.lessons[pos1];
      chromosome.lessons[pos1] = chromosome.lessons[pos2];
      chromosome.lessons[pos2] = lesson;

      // if (lesson) {
      //   const duration = lesson.duration;
      //   const day = this.getRandomInt(days);
      //   const room = this.getRandomInt(numberOfRooms);
      //   const time = this.getRandomInt(dayHours + 1 - duration);
      //   const position = day * numberOfRooms * dayHours + room * dayHours + time;
      //
      //   // replace new lesson to random place
      //   for (let k = duration - 1; k >= 0; k--) {
      //     chromosome.lessons[position + k] = lesson;
      //   }
      // }
    }

    return chromosome;
  }

  logConflicts(chromosome) {
    let score = 0;
    const numberOfRooms = 4;
    const dayHours = 8;
    const daySize = numberOfRooms * dayHours;
    let teacherConflicts = 0;
    let groupConflicts = 0;

    // iterate over days
    for (let i = 0; i < 5; i++) {
      // iterate over rooms
      for (let j = 0; j < numberOfRooms; j++) {
        // iterate over hours
        for (let k = 0; k < dayHours; k++) {
          const currentPosition = i * numberOfRooms * dayHours + j * dayHours + k;
          const currentLesson = chromosome.lessons[currentPosition];

          if (currentLesson) {
            // check overlap of teacher and groups
            let teacherOverLap = false;
            let groupOverLap = false;

            // iterate over rooms for one day
            for (let h = 0; h < numberOfRooms; h++) {
              const position = i * numberOfRooms * dayHours + h * dayHours + k;
              const lesson = chromosome.lessons[position];

              if (h !== j && lesson) {
                if (currentLesson.teacher.teacher.id === lesson.teacher.teacher.id) {
                  teacherOverLap = true;
                  console.log('Teacher Conflicts=> Day: ', i + 1, ' Room1: ', j + 1, ' Room2: ', h + 1, ' Hour: ', k + 1);
                  teacherConflicts++;
                }
                if (currentLesson.group.group.id === lesson.group.group.id) {
                  groupOverLap = true;
                  console.log('Group Conflicts=> Day: ', i + 1, ' Room: ', j + 1, ' Room2: ', h + 1, ' Hour: ', k + 1);
                  groupConflicts++;
                }
              }

              if (teacherOverLap && groupOverLap) {
                break;
              }
            }

            // check for teacher Overlap
            if (!teacherOverLap) {
              score++;
            }

            // check for group Overlap
            if (!groupOverLap) {
              score++;
            }

            // increment for room collapse
            score++;

            // increment for room has available seats
            score++;

            // increment for lesson requires lab and it is in lab or vise-versa
            score++;
          }

        }
      }
    }

    // calculate fitness value => scheduleScore/maxScore where maxScore=numberOfLessons*5
    const numberOfLessons = 150;
    const fitness = score / (numberOfLessons * 5);
    console.log('Teacher Conflicts: ', teacherConflicts);
    console.log('Group Conflicts: ', groupConflicts);
    console.log('All Conflicts: ', teacherConflicts + groupConflicts);
    console.log('Fitness Value: ', fitness);

    return teacherConflicts + groupConflicts;
  }

}
