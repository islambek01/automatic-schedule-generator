import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ShowScheduleService} from '../../show-schedule.service';
import {EditorType} from '../show-schedule/show-schedule.component';
import {TeacherService} from '../../teacher.service';
import {GroupService} from '../../group.service';
import {RoomService} from '../../room.service';
import {LessonService} from '../../lesson.service';

export type EditorType = 'group' | 'teacher' | 'room' | 'lesson' | 'schedule';

@Component({
  selector: 'app-list-schedule',
  templateUrl: './list-schedule.component.html',
  styleUrls: ['./list-schedule.component.scss']
})
export class ListScheduleComponent implements OnInit {
  scheduleForm = this.fb.group({
    name: ['', Validators.required],
    selectedSchedule: {},

  });
  editor: EditorType = 'group';
  generateNewScheduleClicked = false;

  listOfSchedules: any = new Array<object>();
  listOfRooms: any = new Array<object>();
  listOfTeachers: any = new Array<object>();
  listOfGroups: any = new Array<object>();
  listOfLessons: any = new Array<object>();

  listOfTeacherIds: any = new Array<string>();
  listOfGroupIds: any = new Array<string>();

  numberOfRooms: number;
  numberOfTeachers: number;
  numberOfLessons: number;
  numberOfGroups: number;
  numberOfDays: number;
  numberOfHours: number;
  totalLessons: number;

  dayHours: any = new Array<number>(9);
  days: any = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
  info: { space: any, lessons: any };
  conflicts: { total: any, notLocated: any };
  bestSchedule: { lessons: any };
  scheduleByGroups: { lessons: any };
  scheduleByTeachers: { lessons: any };

  constructor(private fb: FormBuilder,
              private lessonService: LessonService,
              private roomService: RoomService,
              private teacherService: TeacherService,
              private groupService: GroupService,
              private showScheduleService: ShowScheduleService) { }

  get showGroupSchedule() {
    return this.editor === 'group';
  }

  get showTeacherSchedule() {
    return this.editor === 'teacher';
  }

  get showRoomSchedule() {
    return this.editor === 'room';
  }

  ngOnInit() {
    this.showScheduleService.getData().subscribe((data) => {
      console.log(data);
      this.listOfSchedules = data.sort((a, b) => (a.name < b.name) ? 1 : -1);
    }, error => {
      console.log(error);
    });

    this.numberOfDays = 5;
    this.numberOfHours = 9;
    this.generateNewScheduleClicked = false;

    this.lessonService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfLessons = data;
      this.numberOfLessons = data.length;
    }, error => {
      console.log(error);
    });

    this.roomService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfRooms = data.sort((a, b) => (a.code > b.code) ? 1 : -1);
      this.numberOfRooms = data.length;
    }, error => {
      console.log(error);
    });

    this.teacherService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfTeachers = data.sort((a, b) => (a.name > b.name) ? 1 : -1);;
      this.numberOfTeachers = data.length;
    }, error => {
      console.log(error);
    });

    this.groupService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfGroups = data.sort((a, b) => (a.name > b.name) ? 1 : -1);
      this.numberOfGroups = data.length;
    }, error => {
      console.log(error);
    });



  }

  toggleEditor(type: EditorType) {
    this.editor = type;
  }

  onShow() {
    const schedule = this.scheduleForm.value;
    console.log(schedule.selectedSchedule)
    let index = this.listOfSchedules.findIndex(p => p.id == schedule.selectedSchedule.id);


    this.bestSchedule = this.listOfSchedules[index].bestSchedule;
    this.generateNewScheduleClicked = true;

    const bestSchedule = this.bestSchedule;

    // console.log('Fitness Value after crossover & mutation: ', maxFitnessValue);
    console.log('The best schedule: ', bestSchedule);

    let c = 0;
    index = 0;
    const lessonOccurence = new Array(this.numberOfLessons);
    lessonOccurence.fill(0);


    // console.log('Empty fields in schedule: ', this.numberOfDays * this.numberOfHours * this.numberOfRooms - c);
    console.log('Occurence of lessons: ', lessonOccurence);

    const conflicts = {
      total: this.logConflicts(bestSchedule),
      notLocated: this.totalLessons - c};
    this.conflicts = conflicts;

    const numberOfDays = this.numberOfDays;
    const numberOfHours = this.numberOfHours;
    const numberOfRooms = this.numberOfRooms;
    const numberOfGroups = this.numberOfGroups;
    const numberOfTeachers = this.numberOfTeachers;
    const scheduleByGroups = {
      lessons: new Array<object>(numberOfGroups * numberOfHours * numberOfDays),
      fitness: 0
    };

    const scheduleByTeachers = {
      lessons: new Array<object>(numberOfTeachers * numberOfHours * numberOfDays),
      fitness: 0
    };

    let q = 0;
    const teacherIds = [];
    const groupIds = [];

    this.listOfTeachers.forEach(teacher => {
      teacherIds.push(teacher.id);
    });

    this.listOfGroups.forEach(group => {
      groupIds.push(group.id);
    });

    this.listOfTeacherIds = teacherIds;
    this.listOfGroupIds = groupIds;

    for (let i = 0; i < numberOfDays; i++) {
      for (let j = 0; j < numberOfRooms; j++) {
        for (let k = 0; k < numberOfHours; k++) {
          const position = i * numberOfHours * numberOfRooms + j * numberOfHours + k;
          const lesson = bestSchedule.lessons[position];

          if (lesson) {
            lesson.roomNum = this.listOfRooms[j];
            const currentGroup = lesson.group[0];
            const currentTeacher = lesson.teacher[0];

            const groupOrder = this.listOfGroupIds.indexOf(currentGroup.id);
            const teacherOrder = this.listOfTeacherIds.indexOf(currentTeacher.id);
            // console.log(currentTeacher, '=> ', teacherOrder)
            // console.log(this.listOfTeachers)

            const pos1 = i * numberOfHours * numberOfGroups + (groupOrder) * numberOfHours + k;
            const pos2 = i * numberOfHours * numberOfTeachers + (teacherOrder) * numberOfHours + k;

            scheduleByGroups.lessons[pos1] = lesson;
            scheduleByTeachers.lessons[pos2] = lesson;
            q++;
          }

        }
      }
    }

    this.scheduleByGroups = scheduleByGroups;
    this.scheduleByTeachers = scheduleByTeachers;

    console.log('Number of lessons in group and teacher schedule: ', q);
    console.log('Number of lessons not located in schedule: ', this.totalLessons - c);
    this.conflicts.notLocated = this.totalLessons - c;

    console.log('Schedule By Groups:', this.scheduleByGroups);

    let total = 0;
    this.listOfLessons.forEach(lesson => {
      total += lesson.duration;
      let numberOfHours = lesson.duration;

    });

    this.totalLessons = total;
  }

  logConflicts(chromosome) {
    let score = 0;
    const numberOfRooms = this.numberOfRooms;
    const numberOfDays = this.numberOfDays;
    const dayHours = this.numberOfHours;
    const daySize = numberOfRooms * dayHours;
    let teacherConflicts = 0;
    let groupConflicts = 0;

    // iterate over days
    for (let i = 0; i < numberOfDays; i++) {
      // iterate over rooms
      for (let j = 0; j < numberOfRooms; j++) {
        // iterate over hours
        for (let k = 0; k < dayHours; k++) {
          const currentPosition = i * numberOfRooms * dayHours + j * dayHours + k;
          const currentLesson = chromosome.lessons[currentPosition];

          if (currentLesson) {
            // check overlap of teacher and groups
            let teacherOverLap = false;
            let groupOverLap = false;

            // iterate over rooms for one day
            for (let h = 0; h < numberOfRooms; h++) {
              const position = i * numberOfRooms * dayHours + h * dayHours + k;
              const lesson = chromosome.lessons[position];

              if (h !== j && lesson) {
                if (currentLesson.teacher[0].id === lesson.teacher[0].id) {
                  teacherOverLap = true;
                  console.log('Teacher Conflicts=> Day: ', i + 1, ' Room1: ', j + 1, ' Room2: ', h + 1, ' Hour: ', k + 1);
                  teacherConflicts++;
                }
                if (currentLesson.group[0].id === lesson.group[0].id) {
                  groupOverLap = true;
                  console.log('Group Conflicts=> Day: ', i + 1, ' Room: ', j + 1, ' Room2: ', h + 1, ' Hour: ', k + 1);
                  groupConflicts++;
                }
              }

              if (teacherOverLap && groupOverLap) {
                break;
              }
            }

            // check for teacher Overlap
            if (!teacherOverLap) {
              score++;
            }

            // check for group Overlap
            if (!groupOverLap) {
              score++;
            }

            // increment for room collapse
            score++;

            // increment for room has available seats
            score++;

            // increment for lesson requires lab and it is in lab or vise-versa
            if ((currentLesson.isLabRequired && this.listOfRooms[j].isLab) || !currentLesson.isLabRequired) {
              score++;
            }
            //  score++;
          }

        }
      }
    }

    // calculate fitness value => scheduleScore/maxScore where maxScore=numberOfLessons*5
    const numberOfLessons = this.totalLessons;
    const fitness = score / (numberOfLessons * 5);
    console.log('Teacher Conflicts: ', teacherConflicts/2);
    console.log('Group Conflicts: ', groupConflicts/2);
    console.log('All Conflicts: ', teacherConflicts/2 + groupConflicts/2);
    console.log('Fitness Value: ', fitness);

    return teacherConflicts/2 + groupConflicts/2;
  }



}
