import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {LessonService} from '../../lesson.service';
import {TeacherService} from '../../teacher.service';
import {GroupService} from '../../group.service';
import {NotifyService} from '../../notify.service';

@Component({
  selector: 'app-lessons',
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.scss']
})
export class LessonsComponent implements OnInit {
  isLoading = false;
  lessonForm = this.fb.group({
    name: ['', Validators.required],
    shortName: [''],
    duration: [''],
    type: [''],
    teacher: {},
    group: {},
    isLabRequired: false,
  });

  listOfLessons: any = new Array<object>();
  listOfTeachers: any = new Array<object>();
  listOfGroups: any = new Array<object>();

  constructor(private fb: FormBuilder,
              private lessonsService: LessonService,
              private teacherService: TeacherService,
              private groupService: GroupService,
              private notify: NotifyService) {
  }

  ngOnInit() {
    this.lessonsService.getData().subscribe((data) => {
      console.log(data);
      this.listOfLessons = data.sort((a, b) => (a.group[0].name < b.group[0].name) ? 1 : -1);
    }, error => {
      console.log(error);
    });

    this.teacherService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfTeachers = data.sort((a, b) => (a.name > b.name) ? 1 : -1);
    }, error => {
      console.log(error);
    });

    this.groupService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfGroups = data.sort((a, b) => (a.name > b.name) ? 1 : -1);
    }, error => {
      console.log(error);
    });
  }

  onAdd() {
    this.isLoading = true;
    const lesson = this.lessonForm.value;
    const teacher = [];
    const group = [];
    teacher.push(lesson.teacher);
    group.push(lesson.group);

    lesson.teacher = teacher;
    lesson.group = group;

    this.lessonsService.createLesson(lesson).then((data) => {
      console.log('lesson was created:', data);
      this.isLoading = false;
      this.notify.update('Lesson: ' + lesson.name + ' saved successfully', 'success');
    });
  }

  remove(lessonId) {
    this.lessonsService.deleteLesson(lessonId).then((data) => {
      console.log(data);
    });
  }

}
