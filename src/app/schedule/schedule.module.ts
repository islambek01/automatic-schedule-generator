import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupsComponent } from './groups/groups.component';
import { TeachersComponent } from './teachers/teachers.component';
import { RoomsComponent } from './rooms/rooms.component';
import { LessonsComponent } from './lessons/lessons.component';
import { ScheduleRoutingModule } from '../schedule-routing.module';
import { HomeComponent } from './home/home.component';
import { ShowScheduleComponent } from './show-schedule/show-schedule.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import { ListScheduleComponent } from './list-schedule/list-schedule.component';

@NgModule({
  declarations: [GroupsComponent, TeachersComponent, RoomsComponent, LessonsComponent, HomeComponent, ShowScheduleComponent, ListScheduleComponent],
  imports: [
    CommonModule,
    BrowserModule,
    ReactiveFormsModule,
    ScheduleRoutingModule
  ]
})
export class ScheduleModule { }
