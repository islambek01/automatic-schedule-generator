import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import {RoomService} from '../../room.service';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {
  roomForm = this.fb.group({
    name:  ['', Validators.required],
    size:  [''],
    isLab: false,
    code:    ['']
  });

  listOfRooms: any  = new Array<object>();

  constructor(private fb: FormBuilder,
              private roomService: RoomService) { }

  ngOnInit() {
    this.roomService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfRooms = data.sort((a, b) => (a.code > b.code) ? 1 : -1);;
    }, error => {
      console.log(error);
    });
  }

  onAdd() {
    const room = this.roomForm.value;

    this.roomService.createRoom(room).then((data) => {
      console.log('room was created:', data);
    });
  }

  remove(roomId) {
    this.roomService.deleteRoom(roomId).then((data) => {
      console.log(data);
    });
  }

}
