import {Component, OnInit} from '@angular/core';
import {LessonService} from '../../lesson.service';
import {RoomService} from '../../room.service';
import {TeacherService} from '../../teacher.service';
import {GroupService} from '../../group.service';
import {FormBuilder, Validators} from '@angular/forms';
import {ShowScheduleService} from '../../show-schedule.service';
import {Observable} from 'rxjs';

export type EditorType = 'group' | 'teacher' | 'room' | 'lesson' | 'schedule';

@Component({
  selector: 'app-show-schedule',
  templateUrl: './show-schedule.component.html',
  styleUrls: ['./show-schedule.component.scss']
})
export class ShowScheduleComponent implements OnInit {
  scheduleForm = this.fb.group({
    name: ['', Validators.required],
    bestSchedule: {},

  });

  editor: EditorType = 'group';
  generateNewScheduleClicked = false;

  listOfLessons: any = new Array<object>();
  listOfRooms: any = new Array<object>();
  listOfTeachers: any = new Array<object>();
  listOfGroups: any = new Array<object>();

  listOfTeacherIds: any = new Array<string>();
  listOfGroupIds: any = new Array<string>();

  numberOfRooms: number;
  numberOfTeachers: number;
  numberOfLessons: number;
  numberOfGroups: number;
  numberOfDays: number;
  numberOfHours: number;
  totalLessons: number;

  dayHours: any = new Array<number>(9);
  days: any = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
  info: { space: any, lessons: any };
  conflicts: { total: any, notLocated: any };
  bestSchedule: { lessons: any };
  scheduleByGroups: { lessons: any };
  scheduleByTeachers: { lessons: any };

  constructor(private fb: FormBuilder,
              private lessonService: LessonService,
              private roomService: RoomService,
              private teacherService: TeacherService,
              private groupService: GroupService,
              private showScheduleService: ShowScheduleService) {
  }

  get showGroupSchedule() {
    return this.editor === 'group';
  }

  get showTeacherSchedule() {
    return this.editor === 'teacher';
  }

  get showRoomSchedule() {
    return this.editor === 'room';
  }

  // makeLesson(i) {
  //   const group = {
  //     name: 'group' + (i % 5 + 1),
  //     id: i % 5 + 1,
  //     size: 25
  //   };
  //
  //   const teacher = {
  //     name: 'teacher' + (i % 5 + 1),
  //     id: i % 5 + 1,
  //     shortName: 'T.R.' + i % 5
  //   };
  //
  //   const room = {
  //     name: 'room' + (i % 5 + 1),
  //     id: i % 5 + 1,
  //     isLab: i % 11 === 0,
  //     size: 30
  //   };
  //
  //   const lesson = {
  //     name: 'lesson' + i,
  //     id: i,
  //     isLabRequired: i % 6 === 0,
  //     duration: 2,
  //     group: { group },
  //     teacher: { teacher },
  //     room: { room }
  //   };
  //
  //   return lesson;
  // }

  ngOnInit() {
    this.numberOfDays = 5;
    this.numberOfHours = 9;
    this.generateNewScheduleClicked = false;

    this.lessonService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfLessons = data;
      this.numberOfLessons = data.length;
    }, error => {
      console.log(error);
    });

    this.roomService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfRooms = data.sort((a, b) => (a.code > b.code) ? 1 : -1);
      this.numberOfRooms = data.length;
    }, error => {
      console.log(error);
    });

    this.teacherService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfTeachers = data.sort((a, b) => (a.name > b.name) ? 1 : -1);;
      this.numberOfTeachers = data.length;
    }, error => {
      console.log(error);
    });

    this.groupService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfGroups = data.sort((a, b) => (a.name > b.name) ? 1 : -1);
      this.numberOfGroups = data.length;
    }, error => {
      console.log(error);
    });
  }

  onSave() {
    const schedule = this.scheduleForm.value;

    const lessons = JSON.parse(JSON.stringify(this.bestSchedule));
    schedule.bestSchedule = lessons;
    // console.warn(group);
    this.showScheduleService.createSchedule(schedule).then((data) => {
      console.log('schedule was created:', data);
    });
  }

  generateNewSchedule() {
    this.generateNewScheduleClicked = true;
    let total = 0;

    const newListOfLessons = [];
    this.listOfLessons.forEach(lesson => {
      total += lesson.duration;
      let numberOfHours = lesson.duration;

      while (numberOfHours > 0) {
        if (lesson.type === 'Single') {
          lesson.duration = 1;
          lesson.id = lesson.id + numberOfHours + this.getRandomInt(1000);
          newListOfLessons.push(lesson);
          numberOfHours--;
        }
        else if (lesson.type === 'Double') {
          lesson.duration = 2;
          lesson.id = lesson.id + numberOfHours + this.getRandomInt(1000);

          newListOfLessons.push(lesson);
          numberOfHours-=2;
        }
        else if (lesson.type === 'Triple') {
          lesson.duration = 3;
          lesson.id = lesson.id + numberOfHours + this.getRandomInt(1000);
          newListOfLessons.push(lesson);
          numberOfHours-=3;
        }
      }
    });

    this.totalLessons = total;

    const allLessons = newListOfLessons;

    let maxFitnessValue = 0;
    let minFitnessValue = 1;

    let population = [];
    for (let k = 1; k < 100001; k++) {
      const chromosome = this.makeNewChromosome(allLessons);

      if (chromosome.fitness > maxFitnessValue) {
        maxFitnessValue = chromosome.fitness;
        this.bestSchedule = chromosome;
      }

      if (chromosome.fitness < minFitnessValue) {
        minFitnessValue = chromosome.fitness;
      }

      population.push(chromosome);
    }
    // console.log(population);

    console.log('Max Fitness Value: ', maxFitnessValue);
    console.log('Min Fitness Value: ', minFitnessValue);
    console.log('Please wait for 50 seconds to see final result...');

    const sortedPopulation = population.sort(function (a, b) {
      return b.fitness - a.fitness;
    });

    population = sortedPopulation;

    console.log('Sorted Population:', sortedPopulation);

    for (let i = 1; i < 50000; i++) {
      // select two parent chromosomes randomly to make crossover
      const first = this.getRandomInt(200);
      const second = this.getRandomInt(200);

      if (first !== second) {
        const parent1 = population[first];
        const parent2 = population[second];

        if (parent1 && parent2) {
          const newOffSprings = this.makeCrossover(parent1, parent2);
          const offSpring1 = newOffSprings[0];
          const offSpring2 = newOffSprings[1];

          // const newChromosome1 = this.makeMutation(offSpring1);
          // const newChromosome2 = this.makeMutation(offSpring2);
          //
          // const fitnessVal1 = this.calculateFitness(newChromosome1);
          // newChromosome1.fitness = fitnessVal1;
          //
          // const fitnessVal2 = this.calculateFitness(newChromosome2);
          // newChromosome2.fitness = fitnessVal2;
          //
          // // console.log(parent1.fitness, ' :S: ', offSpring1.fitness);
          // // console.log(parent2.fitness, ' :2S: ', offSpring2.fitness);
          // console.log(offSpring1.fitness, ' :VS: ', newChromosome1.fitness);
          //
          // if (offSpring1.fitness < newChromosome1.fitness) {
          //   console.log(offSpring1.fitness, ' :VS: ', newChromosome1.fitness);
          // }
          //
          // if (offSpring2.fitness < newChromosome2.fitness) {
          //   console.log(offSpring2.fitness, ' :2VS: ', newChromosome2.fitness);
          // }

          if (offSpring1.fitness > maxFitnessValue) {
            maxFitnessValue = offSpring1.fitness;
            this.bestSchedule = offSpring1;
          }
          if (offSpring2.fitness > maxFitnessValue) {
            maxFitnessValue = offSpring2.fitness;
            this.bestSchedule = offSpring2;
          }

          if (parent1.fitness < offSpring1.fitness) {
            population[first] = offSpring1;
            // console.log(parent1.fitness, ' 1VS1 ', offSpring1.fitness);
          } else if (parent1.fitness < offSpring2.fitness) {
            population[first] = offSpring2;
            // console.log(parent1.fitness, ' 1VS2 ', offSpring2.fitness);
          } else if (parent2.fitness < offSpring1.fitness) {
            population[second] = offSpring1;
            // console.log(parent2.fitness, ' 2VS1 ', offSpring1.fitness);
          } else if (parent2.fitness < offSpring2.fitness) {
            population[second] = offSpring2;
            // console.log(parent2.fitness, ' 2VS2 ', offSpring2.fitness);
          }
        }

      }

    }

    const bestSchedule = this.bestSchedule;
    // console.log('Fitness Value after crossover & mutation: ', maxFitnessValue);
    console.log('The best schedule: ', bestSchedule);

    let c = 0;
    let index = 0;
    const lessonOccurence = new Array(this.numberOfLessons);
    lessonOccurence.fill(0);
    bestSchedule.lessons.forEach(lesson => {
      if (lesson) {
        lessonOccurence[index]++;
        c++;
      }
      index++;
    });

    // console.log('Empty fields in schedule: ', this.numberOfDays * this.numberOfHours * this.numberOfRooms - c);
    console.log('Occurence of lessons: ', lessonOccurence);

    const conflicts = {
      total: this.logConflicts(bestSchedule),
      notLocated: this.totalLessons - c};
    this.conflicts = conflicts;

    const numberOfDays = this.numberOfDays;
    const numberOfHours = this.numberOfHours;
    const numberOfRooms = this.numberOfRooms;
    const numberOfGroups = this.numberOfGroups;
    const numberOfTeachers = this.numberOfTeachers;

    const scheduleByGroups = {
      lessons: new Array<object>(numberOfGroups * numberOfHours * numberOfDays),
      fitness: 0
    };

    const scheduleByTeachers = {
      lessons: new Array<object>(numberOfTeachers * numberOfHours * numberOfDays),
      fitness: 0
    };

    let q = 0;
    const teacherIds = [];
    const groupIds = [];

    this.listOfTeachers.forEach(teacher => {
      teacherIds.push(teacher.id);
    });

    this.listOfGroups.forEach(group => {
      groupIds.push(group.id);
    });

    this.listOfTeacherIds = teacherIds;
    this.listOfGroupIds = groupIds;

    for (let i = 0; i < numberOfDays; i++) {
      for (let j = 0; j < numberOfRooms; j++) {
        for (let k = 0; k < numberOfHours; k++) {
          const position = i * numberOfHours * numberOfRooms + j * numberOfHours + k;
          const lesson = bestSchedule.lessons[position];

          if (lesson) {
            lesson.roomNum = this.listOfRooms[j];
            const currentGroup = lesson.group[0];
            const currentTeacher = lesson.teacher[0];

            const groupOrder = this.listOfGroupIds.indexOf(currentGroup.id);
            const teacherOrder = this.listOfTeacherIds.indexOf(currentTeacher.id);
            // console.log(currentTeacher, '=> ', teacherOrder)
            // console.log(this.listOfTeachers)

            const pos1 = i * numberOfHours * numberOfGroups + (groupOrder) * numberOfHours + k;
            const pos2 = i * numberOfHours * numberOfTeachers + (teacherOrder) * numberOfHours + k;

            scheduleByGroups.lessons[pos1] = lesson;
            scheduleByTeachers.lessons[pos2] = lesson;
            q++;
          }

        }
      }
    }

    this.scheduleByGroups = scheduleByGroups;
    this.scheduleByTeachers = scheduleByTeachers;

    console.log('Number of lessons in group and teacher schedule: ', q);
    console.log('Number of lessons not located in schedule: ', this.totalLessons - c);
    this.conflicts.notLocated = this.totalLessons - c;

    console.log('Schedule By Groups:', this.scheduleByGroups);
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  makeNewChromosome(allLessons) {
    const numberOfRooms = this.numberOfRooms;
    const dayHours = this.numberOfHours;
    const days = this.numberOfDays;
    const chromosome = {
      lessons: new Array<object>(numberOfRooms * dayHours * days),
      fitness: 0
    };

    allLessons.forEach(lesson => {
      const duration = lesson.duration;
      const day = this.getRandomInt(days);
      const room = this.getRandomInt(numberOfRooms);
      const time = this.getRandomInt(dayHours + 1 - duration);
      const position = day * numberOfRooms * dayHours + room * dayHours + time;

      // const position = this.getRandomInt(days * dayHours * numberOfRooms) - duration;
      for (let k = duration - 1; k >= 0; k--) {
        chromosome.lessons[position + k] = lesson;
      }
    });

    const fitnessValue = this.calculateFitness(chromosome);
    chromosome.fitness = fitnessValue;

    return chromosome;
  }

  calculateFitness(chromosome) {
    let score = 0;
    const numberOfRooms = this.numberOfRooms;
    const numberOfDays = this.numberOfDays;
    const dayHours = this.numberOfHours;
    const daySize = numberOfRooms * dayHours;
    const listOfRooms = this.listOfRooms;

    // iterate over days
    for (let i = 0; i < numberOfDays; i++) {
      // iterate over rooms
      for (let j = 0; j < numberOfRooms; j++) {
        // iterate over hours
        for (let k = 0; k < dayHours; k++) {
          const currentPosition = i * numberOfRooms * dayHours + j * dayHours + k;
          const currentLesson = chromosome.lessons[currentPosition];

          // console.log('positions ', currentPosition)
          if (currentLesson) {
            // check overlap of teacher and groups
            let teacherOverLap = false;
            let groupOverLap = false;

            // iterate over rooms for one day
            for (let h = 0; h < numberOfRooms; h++) {
              const position = i * numberOfRooms * dayHours + h * dayHours + k;
              const lesson = chromosome.lessons[position];

              if (h !== j && lesson) {
                if (currentLesson.teacher[0].id === lesson.teacher[0].id) {
                  teacherOverLap = true;
                }
                if (currentLesson.group[0].id === lesson.group[0].id) {
                  groupOverLap = true;
                }
              }

              if (teacherOverLap && groupOverLap) {
                break;
              }
            }

            // check for teacher Overlap
            if (!teacherOverLap) {
              score++;
            }

            // check for group Overlap
            if (!groupOverLap) {
              score++;
            }

            // increment for room collapse
            score++;

            // increment for room has available seats
            score++;

            // increment for lesson requires lab and it is in lab or vise-versa

            if ((currentLesson.isLabRequired && listOfRooms[j].isLab) || !currentLesson.isLabRequired) {
              score++;
            }
          }

        }
      }
    }

    // calculate fitness value => scheduleScore/maxScore where maxScore=numberOfLessons*5
    const totalLessons = this.totalLessons;
    const fitness = score / (totalLessons * 5);

    return fitness;
  }

  makeCrossover(parent1, parent2) {
    const numberOfCrossoverPoints = 2;
    const numberOfMutationSize = 2;
    const numberOfLessons = this.numberOfLessons;
    const numberOfRooms = this.numberOfRooms;
    const dayHours = this.numberOfHours;
    const days = this.numberOfDays;

    const offSpring1 = {
      lessons: new Array<{ id: any }>(numberOfRooms * dayHours * days),
      fitness: 0
    };

    const offSpring2 = {
      lessons: new Array<{ id: any }>(numberOfRooms * dayHours * days),
      fitness: 0
    };

    const crossoverPoint = this.getRandomInt(numberOfRooms * dayHours * days);

    for (let k = 0; k < crossoverPoint; k++) {

      const lesson1 = parent1.lessons[k];
      const lesson2 = parent2.lessons[k];

      let notChanged1 = true;
      let notChanged2 = true;

      if (lesson1) {
        let occurence1 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring2.lessons[q];

          if (lesson && lesson.id === lesson1.id) {
            occurence1++;
          }
        }

        if (occurence1 < lesson1.duration) {
          offSpring2.lessons[k] = lesson1;
          notChanged1 = false;
        }
      }
      if (notChanged1 && lesson2) {
        let occurence1 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring2.lessons[q];

          if (lesson && lesson.id === lesson2.id) {
            occurence1++;
          }
        }

        if (occurence1 < lesson2.duration) {
          offSpring2.lessons[k] = lesson2;
        }
      }

      if (lesson2) {
        let occurence2 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring1.lessons[q];

          if (lesson && lesson.id === lesson2.id) {
            occurence2++;
          }
        }

        if (occurence2 < lesson2.duration) {
          offSpring1.lessons[k] = lesson2;
          notChanged2 = false;
        }
      }
      if (notChanged2 && lesson1) {
        let occurence2 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring1.lessons[q];

          if (lesson && lesson.id === lesson1.id) {
            occurence2++;
          }
        }

        if (occurence2 < lesson1.duration) {
          offSpring1.lessons[k] = lesson1;
        }
      }

      // offSpring1.lessons[k] = parent2.lessons[k] ? parent2.lessons[k] : parent1.lessons[k];
      // offSpring2.lessons[k] = parent1.lessons[k] ? parent1.lessons[k] : parent2.lessons[k];
    }

    for (let k = crossoverPoint; k < numberOfRooms * dayHours * days; k++) {
      const lesson1 = parent1.lessons[k];
      const lesson2 = parent2.lessons[k];

      let notChanged1 = true;
      let notChanged2 = true;
      if (lesson1) {
        let occurence1 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring1.lessons[q];

          if (lesson && lesson.id === lesson1.id) {
            occurence1++;
          }
        }

        if (occurence1 < lesson1.duration) {
          offSpring1.lessons[k] = lesson1;
          notChanged1 = false;
        }
      }
      if (notChanged1 && lesson2) {
        let occurence1 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring1.lessons[q];

          if (lesson && lesson.id === lesson2.id) {
            occurence1++;
          }
        }

        if (occurence1 < lesson2.duration) {
          offSpring1.lessons[k] = lesson2;
        }
      }

      if (lesson2) {
        let occurence2 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring2.lessons[q];

          if (lesson && lesson.id === lesson2.id) {
            occurence2++;
          }
        }

        if (occurence2 < lesson2.duration) {
          offSpring2.lessons[k] = lesson2;
          notChanged2 = false;
        }
      }
      if (notChanged2 && lesson1) {
        let occurence2 = 0;
        for (let q = 0; q < k; q++) {
          const lesson = offSpring2.lessons[q];

          if (lesson && lesson.id === lesson1.id) {
            occurence2++;
          }
        }

        if (occurence2 < lesson1.duration) {
          offSpring2.lessons[k] = lesson1;
        }
      }

      // offSpring1.lessons[k] = parent1.lessons[k] ? parent1.lessons[k] : parent2.lessons[k];
      // offSpring2.lessons[k] = parent2.lessons[k] ? parent2.lessons[k] : parent1.lessons[k];
    }

    const fitnessValue1 = this.calculateFitness(offSpring1);
    offSpring1.fitness = fitnessValue1;

    const fitnessValue2 = this.calculateFitness(offSpring2);
    offSpring2.fitness = fitnessValue2;



    if (parent1.fitness < offSpring1.fitness) {
      // console.log(parent1.fitness, ' :QQ: ', offSpring1.fitness)

    }
    if (parent2.fitness < offSpring2.fitness) {
      // console.log(parent2.fitness, ' :QQ: ', offSpring2.fitness)
      // debugger
    }
    return ([offSpring1, offSpring2]);
  }

  makeMutation(chromosome) {
    const mutationSize = 10;
    const numberOfRooms = this.numberOfRooms;
    const dayHours = this.numberOfHours;
    const days = this.numberOfDays;

    for (let i = mutationSize; i >= 0; i--) {
      // select lesson randomly
      const pos1 = this.getRandomInt(numberOfRooms * dayHours * days);
      const pos2 = this.getRandomInt(numberOfRooms * dayHours * days);

      const lesson = chromosome.lessons[pos1];
      chromosome.lessons[pos1] = chromosome.lessons[pos2];
      chromosome.lessons[pos2] = lesson;

      // if (lesson) {
      //   const duration = lesson.duration;
      //   const day = this.getRandomInt(days);
      //   const room = this.getRandomInt(numberOfRooms);
      //   const time = this.getRandomInt(dayHours + 1 - duration);
      //   const position = day * numberOfRooms * dayHours + room * dayHours + time;
      //
      //   // replace new lesson to random place
      //   for (let k = duration - 1; k >= 0; k--) {
      //     chromosome.lessons[position + k] = lesson;
      //   }
      // }
    }

    return chromosome;
  }

  logConflicts(chromosome) {
    let score = 0;
    const numberOfRooms = this.numberOfRooms;
    const numberOfDays = this.numberOfDays;
    const dayHours = this.numberOfHours;
    const daySize = numberOfRooms * dayHours;
    let teacherConflicts = 0;
    let groupConflicts = 0;

    // iterate over days
    for (let i = 0; i < numberOfDays; i++) {
      // iterate over rooms
      for (let j = 0; j < numberOfRooms; j++) {
        // iterate over hours
        for (let k = 0; k < dayHours; k++) {
          const currentPosition = i * numberOfRooms * dayHours + j * dayHours + k;
          const currentLesson = chromosome.lessons[currentPosition];

          if (currentLesson) {
            // check overlap of teacher and groups
            let teacherOverLap = false;
            let groupOverLap = false;

            // iterate over rooms for one day
            for (let h = 0; h < numberOfRooms; h++) {
              const position = i * numberOfRooms * dayHours + h * dayHours + k;
              const lesson = chromosome.lessons[position];

              if (h !== j && lesson) {
                if (currentLesson.teacher[0].id === lesson.teacher[0].id) {
                  teacherOverLap = true;
                  console.log('Teacher Conflicts=> Day: ', i + 1, ' Room1: ', j + 1, ' Room2: ', h + 1, ' Hour: ', k + 1);
                  teacherConflicts++;
                }
                if (currentLesson.group[0].id === lesson.group[0].id) {
                  groupOverLap = true;
                  console.log('Group Conflicts=> Day: ', i + 1, ' Room: ', j + 1, ' Room2: ', h + 1, ' Hour: ', k + 1);
                  groupConflicts++;
                }
              }

              if (teacherOverLap && groupOverLap) {
                break;
              }
            }

            // check for teacher Overlap
            if (!teacherOverLap) {
              score++;
            }

            // check for group Overlap
            if (!groupOverLap) {
              score++;
            }

            // increment for room collapse
            score++;

            // increment for room has available seats
            score++;

            // increment for lesson requires lab and it is in lab or vise-versa
            if ((currentLesson.isLabRequired && this.listOfRooms[j].isLab) || !currentLesson.isLabRequired) {
              score++;
            }
          //  score++;
          }

        }
      }
    }

    // calculate fitness value => scheduleScore/maxScore where maxScore=numberOfLessons*5
    const numberOfLessons = this.totalLessons;
    const fitness = score / (numberOfLessons * 5);
    console.log('Teacher Conflicts: ', teacherConflicts/2);
    console.log('Group Conflicts: ', groupConflicts/2);
    console.log('All Conflicts: ', teacherConflicts/2 + groupConflicts/2);
    console.log('Fitness Value: ', fitness);

    return teacherConflicts/2 + groupConflicts/2;
  }

  toggleEditor(type: EditorType) {
    this.editor = type;
  }

}
