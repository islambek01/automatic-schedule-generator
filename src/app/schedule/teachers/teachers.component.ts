import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import {TeacherService} from '../../teacher.service';

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss']
})
export class TeachersComponent implements OnInit {
  teacherForm = this.fb.group({
    name:      ['', Validators.required],
    surName:   [''],
    shortName: [''],
    code:        ['']
  });

  listOfTeachers: any  = new Array<object>();

  constructor(private fb: FormBuilder,
              private teacherService: TeacherService) { }

  ngOnInit() {
    this.teacherService.getData().subscribe((data) => {
      // console.log(data);
      this.listOfTeachers = data.sort((a, b) => (a.name > b.name) ? 1 : -1);;
    }, error => {
      console.log(error);
    });
  }

  onAdd() {
    const teacher = this.teacherForm.value;
    const code = this.listOfTeachers.length;
    teacher.code = code;
    this.teacherService.createTeacher(teacher).then((data) => {
      console.log('teacher was created:', data);
    });
  }

  remove(teacherId) {
    this.teacherService.deleteTeacher(teacherId).then((data) => {
      console.log(data);
    });
  }

}
