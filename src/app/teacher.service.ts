import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  notesCollection: AngularFirestoreCollection<any>;
  noteDocument: AngularFirestoreDocument<any>;

  constructor(private afs: AngularFirestore) {
    this.notesCollection = this.afs.collection('teachers');
  }

  getData(): Observable<any[]> {
    return this.notesCollection.snapshotChanges().pipe(
      map((actions) => {
        // console.log(actions);
        return actions.map((a) => {
          // console.log(a);
          const data = a.payload.doc.data();
          return {id: a.payload.doc.id, ...data};
        });
      })
    );
  }

  getTeacher(id: string) {
    return this.afs.doc<any>(`teachers/${id}`);
  }

  createTeacher(group: any) {
    return this.notesCollection.add(group);
  }

  updateTeacher(id: string, data: any) {
    return this.getTeacher(id).update(data);
  }

  deleteTeacher(id: string) {
    return this.getTeacher(id).delete();
  }
}
