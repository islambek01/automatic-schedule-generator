import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LessonService {
  lessonsCollection: AngularFirestoreCollection<any>;
  noteDocument: AngularFirestoreDocument<any>;

  constructor(private afs: AngularFirestore) {
    this.lessonsCollection = this.afs.collection('lessons');
  }

  getData(): Observable<any[]> {
    return this.lessonsCollection.snapshotChanges().pipe(
      map((actions) => {
        // console.log(actions);
        return actions.map((a) => {
          // console.log(a);
          const data = a.payload.doc.data();
          return {id: a.payload.doc.id, ...data};
        });
      })
    );
  }

  getLesson(id: string) {
    return this.afs.doc<any>(`lessons/${id}`);
  }

  createLesson(group: any) {
    return this.lessonsCollection.add(group);
  }

  updateLesson(id: string, data: any) {
    return this.getLesson(id).update(data);
  }

  deleteLesson(id: string) {
    return this.getLesson(id).delete();
  }
}
