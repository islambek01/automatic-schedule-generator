import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  notesCollection: AngularFirestoreCollection<any>;
  noteDocument: AngularFirestoreDocument<any>;

  constructor(private afs: AngularFirestore) {
    this.notesCollection = this.afs.collection('groups');
  }

  getData(): Observable<any[]> {
    return this.notesCollection.snapshotChanges().pipe(
      map((actions) => {
        // console.log(actions);
        return actions.map((a) => {
          // console.log(a);
          const data = a.payload.doc.data();
          return {id: a.payload.doc.id, ...data};
        });
      })
    );
  }

  getNote(id: string) {
    return this.afs.doc<any>(`groups/${id}`);
  }

  createNote(group: any) {
    return this.notesCollection.add(group);
  }

  updateNote(id: string, data: any) {
    return this.getNote(id).update(data);
  }

  deleteNote(id: string) {
    return this.getNote(id).delete();
  }
}
