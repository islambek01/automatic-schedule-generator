import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoomService {
  roomsCollection: AngularFirestoreCollection<any>;
  noteDocument: AngularFirestoreDocument<any>;

  constructor(private afs: AngularFirestore) {
    this.roomsCollection = this.afs.collection('rooms');
  }

  getData(): Observable<any[]> {
    return this.roomsCollection.snapshotChanges().pipe(
      map((actions) => {
        // console.log(actions);
        return actions.map((a) => {
          // console.log(a);
          const data = a.payload.doc.data();
          return {id: a.payload.doc.id, ...data};
        });
      })
    );
  }

  getRoom(id: string) {
    return this.afs.doc<any>(`rooms/${id}`);
  }

  createRoom(group: any) {
    return this.roomsCollection.add(group);
  }

  updateRoom(id: string, data: any) {
    return this.getRoom(id).update(data);
  }

  deleteRoom(id: string) {
    return this.getRoom(id).delete();
  }
}
