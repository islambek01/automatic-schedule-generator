import { TestBed } from '@angular/core/testing';

import { ShowScheduleService } from './show-schedule.service';

describe('ShowScheduleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShowScheduleService = TestBed.get(ShowScheduleService);
    expect(service).toBeTruthy();
  });
});
