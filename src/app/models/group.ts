export class Group {
  id: string;
  code: number;
  name: string;
  size: number;
}
