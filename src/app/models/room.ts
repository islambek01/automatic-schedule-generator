export class Room {
  get isLab(): boolean {
    return this._isLab;
  }
  id: string;
  code: string;
  name: string;
  private _isLab: boolean;
  size: number;
}
