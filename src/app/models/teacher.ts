export class Teacher {
  get surName(): string {
    return this._surName;
  }
  get shortName(): string {
    return this._shortName;
  }
  id: string;
  code: number;
  name: string;
  private _surName: string;
  private _shortName: string;
}
