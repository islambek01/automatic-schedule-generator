import { Teacher } from './teacher';
import { Group} from './group';

export class Lesson {
  get isLabRequired(): boolean {
    return this._isLabRequired;
  }
  get shortName(): string {
    return this._shortName;
  }
  id: number;
  name: string;
  private _shortName: string;
  private _isLabRequired: boolean;
  duration: number;
  teacher: Teacher;
  group: Group;
}
